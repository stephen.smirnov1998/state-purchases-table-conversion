#!/usr/bin/python

# a command to convert xls to xlsx on linux with Libreoffice installed is 
# $ soffice --convert-to xlsx test.xls
# batch operation is supported via star (*.xls)

# width 100 almost exactly corresponds to 18X5=90 chars of 12 size noto sans mono
# which means height of 12.5 per row and width 100/90~=1.11 per char

#from openpyxl.utils import column_index_from_string
from openpyxl.styles import Alignment, Font
from openpyxl import load_workbook
from openpyxl import Workbook
import sys
import requests

target_file = ''
output_file = ''
suspects = []
cmd = sys.argv[0]
rusprofile_requests=False

# ------------------ Navigation constants---------------------------------

DATE_COL = 'A'
NUMBER_COL = 'B'
TITLE_COL = 'D'
COMPETITOR_TITLE_COL = 'J'
COMPETITOR_INN_COL = 'K'
COMPETITOR_PRICE_COL = 'Q'
INITIAL_PRICE_COL = 'P'
PRICE_DROP_COL = 'R'
COMPETITOR_NUMBER_COL = 'S'

OUT_DATE_COL = 'C'
OUT_NUMBER_COL = 'A'
OUT_TITLE_COL = 'B'
OUT_COMPETITOR_TITLE_COL = 'G'
OUT_COMPETITOR_PRICE_COL = 'H'
OUT_INITIAL_PRICE_COL = 'D'
OUT_WINNER_PRICE_COL = 'F'
OUT_WINNER_TITLE_COL = 'E'
OUT_PRICE_DROP_COL = 'I'

COMPLIST_TITLE_COL = 'A'
COMPLIST_INN_COL = 'B'
COMPLIST_DIRECTOR_COL = 'C'
COMPLIST_FOUNDER_COL = 'D'
COMPLIST_SHARE_COL = 'E'
COMPLIST_PHONE_COL = 'F'
COMPLIST_SOURCE_COL = 'G'


# ------------------ Visual constants---------------------------------

COMPANY_TITLE_COL_WIDTH_MAX = 50
TITLE_COL_WIDTH_MAX = 100
COMPANY_TITLE_COLS = [OUT_COMPETITOR_TITLE_COL, OUT_WINNER_TITLE_COL]
PRICE_COLS = (OUT_INITIAL_PRICE_COL, OUT_COMPETITOR_PRICE_COL, OUT_WINNER_PRICE_COL)
NUMBER_COLS = PRICE_COLS + (OUT_NUMBER_COL,OUT_DATE_COL,OUT_PRICE_DROP_COL)
HEIGHT_PER_ROW = 12.5
INTERROW_HEIGHT = HEIGHT_PER_ROW*(20/23)
WIDTH_PER_CHAR = 105.0/90.0
HMARGIN = 2

font = Font(name='Noto Sans Mono',
    size=12,
    bold=False,
    italic=False,
    vertAlign=None,
    underline='none',
    strike=False,
    color='FF000000')

title_font = Font(name='Noto Sans Mono',
    size=12,
    bold=True,
    italic=False,
    vertAlign=None,
    underline='none',
    strike=False,
    color='FF000000')

alignment=Alignment(horizontal='center',
    vertical='center',
    text_rotation=0,
    wrap_text=True,
    shrink_to_fit=False,
    indent=0)

# ------------------ Network constants---------------------------------

RUSPROFILE_SHORT='rusprofile.ru'
RUSPROFILE_COMMON = "https://www.rusprofile.ru/id/"
YAHOOCOJP_COMMON = "https://search.yahoo.co.jp/search;?p="

def validate_number(number):
    # print (len(number))
    if number.isnumeric() and len(number)==19:
        return 1
    # print(str(number)+' is not a valid regnumber')
    return 0

def validate_inn(inn):
    if inn.isnumeric() and len(inn) in [10, 12]: # ten for juridical and 12 for physical
        return 1
    return 0

def process_entry (ws, index, out_ws, suspect_list):
    comps=int(ws[COMPETITOR_NUMBER_COL+str(index)].value)
    
    # validating entry first
    if comps==0:
        return
    # print ('competitors '+str(comps))
    # print (ws[(COMPETITOR_INN_COL+str(index+1)):(COMPETITOR_INN_COL+str(index+comps))])
    for c in ws[(COMPETITOR_INN_COL+str(index)):(COMPETITOR_INN_COL+str(index+comps))]:
        # print (c)
        if str(c[0].value).lstrip('\'') not in suspect_list:
            print (str(c[0].value)+ " is not suspect, not writing")
            return
    # writing down successful entry
    out_index=len(out_ws['A:A'])+1
    print ('writing at index '+str(out_index))
    out_ws[OUT_DATE_COL+str(out_index)]=ws[DATE_COL+str(index)].value
    out_ws[OUT_NUMBER_COL+str(out_index)]=ws[NUMBER_COL+str(index)].value
    out_ws[OUT_TITLE_COL+str(out_index)]=ws[TITLE_COL+str(index)].value
    out_ws[OUT_WINNER_TITLE_COL+str(out_index)]=ws[COMPETITOR_TITLE_COL+str(index)].value
    out_ws[OUT_PRICE_DROP_COL+str(out_index)]=ws[PRICE_DROP_COL+str(index)].value
    out_ws[OUT_WINNER_PRICE_COL+str(out_index)]=ws[COMPETITOR_PRICE_COL+str(index)].value
    out_ws[OUT_INITIAL_PRICE_COL+str(out_index)]=ws[INITIAL_PRICE_COL+str(index)].value
            
    compressed_titles = ''
    for c in ws[(COMPETITOR_TITLE_COL+str(index+1)):(COMPETITOR_TITLE_COL+str(index+comps))]:
        compressed_titles += c[0].value+','+'\n'
    
    out_ws[OUT_COMPETITOR_TITLE_COL+str(out_index)] =  compressed_titles.rstrip('\n').rstrip(',')
    
    compressed_prices = ''
    for c in ws[(COMPETITOR_PRICE_COL+str(index+1)):(COMPETITOR_PRICE_COL+str(index+comps))]:
        compressed_prices += str(c[0].value)+'\n'
    
    out_ws[OUT_COMPETITOR_PRICE_COL+str(out_index)] =  compressed_prices.rstrip('\n')


def print_help():
    print()
    print("Usage: %s [OPTION] SOURCE DEST <suspect-inns ...>" % (cmd))
    print('load anti-cartel form SOURCE, filter it according to suspects and save it as DEST')
    print()
    print('Options:')
    print('\t -h,-H \t --help \t print this help message and do nothing')
    print('\t  \t --rusprofile \t try fetching company data from rusprofile.ru')
    pass

def prettify_out_ws_2(ws):

    ws.column_dimensions[OUT_TITLE_COL].width=TITLE_COL_WIDTH_MAX
    for col in COMPANY_TITLE_COLS:
        ws.column_dimensions[col].width=COMPANY_TITLE_COL_WIDTH_MAX
    
    for col in ws.iter_cols():
        for cell in col:
            cell.font=font
            cell.alignment=alignment
    for cell in ws['1:1']:
        cell.font=title_font
        
    for col in NUMBER_COLS:
        autofit_column(ws,col)

    for row_index in range(ws.min_row,ws.max_row+1):
        autofit_row(ws,row_index)
    
    pass

def autofit_column(ws,letter):
    col=ws[letter+':'+letter]
    charnum = 0
    for cell in col:
        string = str(cell.value)
        tokens = string.split('\n')
        for t in tokens:
            if len(t)>charnum:
                charnum=len(t)
    ws.column_dimensions[letter].width = WIDTH_PER_CHAR*charnum+HMARGIN
    pass

def autofit_row(ws,index):
    row = ws[str(index)+':'+str(index)]
    rownum=0
    for cell in row:
        string = str(cell.value)
        tokens = string.split('\n')
        rows_needed = 0
        for t in tokens:
            # print(cell.column)
            colwidth = ws.column_dimensions[cell.column].width
            token_width = len(str(t))*WIDTH_PER_CHAR
            rows_needed+= (-1)*(((-1)*token_width)//(colwidth-1))
        if rows_needed > rownum:
            rownum = rows_needed
    ws.row_dimensions[index].height = rownum*HEIGHT_PER_ROW + (rownum-1)*INTERROW_HEIGHT
        

def transform_table (in_path, out_path, suspect_list):
    
    print('openining %s' % in_path)
    wb = load_workbook(in_path)
    ws = wb['Победители по ФЗ 44']
    limit = len(ws[COMPETITOR_TITLE_COL+':'+COMPETITOR_TITLE_COL]) #it is the lengthiest column
    
    out_wb = Workbook()
    prepare_wb(out_wb, suspect_list)
    out_ws = out_wb['лист 2 (торги)']
    
    i=2
    while i != limit:
        number = ''
        candidate = ws[NUMBER_COL+str(i)].value
        if isinstance(candidate, str):
            number = candidate.lstrip(' ').rstrip(' ').rstrip('\n')
        if validate_number(number):
            print (str(i)+': '+number)
            process_entry(ws, i, out_ws, suspect_list)
        i += 1
    prettify_out_ws_2(out_ws)
    out_wb.save(out_path)
    

def prepare_wb (workbook, suspect_list):
    ws = workbook.active
    ws.title = 'лист 1 (компании)'
    ws2 = workbook.create_sheet('лист 2 (торги)')
    ws2[OUT_NUMBER_COL+'1'] = 'Номер извещения'
    ws2[OUT_TITLE_COL+'1'] = 'Наименование закупки'
    ws2[OUT_DATE_COL+'1'] = 'Дата публикации'
    ws2[OUT_INITIAL_PRICE_COL+'1'] = 'НМЦ (руб.)'
    ws2[OUT_WINNER_TITLE_COL+'1'] = 'Победитель'
    ws2[OUT_WINNER_PRICE_COL+'1'] = 'Цена победителя'
    ws2[OUT_COMPETITOR_TITLE_COL+'1'] = 'Другие участники'
    ws2[OUT_COMPETITOR_PRICE_COL+'1'] = 'Цена'
    ws2[OUT_PRICE_DROP_COL+'1'] = 'Снижение, %'
    
    ws['A1'] = 'Приложение, Таблица 1 на двух листах '
    ws[COMPLIST_TITLE_COL+'2'] = 'Компания'
    ws[COMPLIST_INN_COL+'2'] = 'ИНН'
    ws[COMPLIST_DIRECTOR_COL+'2'] = 'Генеральный директор'
    ws[COMPLIST_FOUNDER_COL+'2'] = 'Учредитель'
    ws[COMPLIST_SHARE_COL+'2'] = 'Доля, %'
    ws[COMPLIST_PHONE_COL+'2'] = 'Телефон'
    ws[COMPLIST_SOURCE_COL+'2'] = 'Источник'
    
    index=3
    for suspect in suspect_list:
        ws[COMPLIST_INN_COL+str(index)] = suspect
        if rusprofile_requests:
            rusprofile_extract_info(ws,index,suspect)
        index+=1

def rusprofile_extract_info(ws,index,inn):
    
    query_result = load_url(YAHOOCOJP_COMMON+RUSPROFILE_SHORT+'+'+str(inn))
    #print(query_result)
    start = query_result.find(RUSPROFILE_COMMON.replace('https://',''))
    #print ('startposition = ', str(start))
    end= query_result.find('"',start)
    #print ('endposition = ', str(end))
    link = query_result[start:end]
    if link == '':
        print("can't find rusprofile link for " + str(inn) + ' with yahoo.co.jp')
    else:
        print("rusprofile link found for " + str(inn))
        ws[COMPLIST_SOURCE_COL+str(index)] = link
        print(link)
     
def load_url(url):
    try:
        print ('loading url: '+url)
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:39.0)'}
        res = requests.get(url, headers=headers, timeout=60)
        res.raise_for_status()
        return res.text

    except Exception as e:
        print(url+'not responding')
        print(e)


index = 1

if len(sys.argv) == 1:
    print('Error: need two arguments + suspects')
    print_help()
    sys.exit(1)

if len(sys.argv) == 2 :
    if sys.argv[1] in ['-h', '-H','--help']:
        print_help()
        sys.exit(0)
    else:
        print('Error: need two arguments + suspects')
        print_help()
        sys.exit(1)

while index < len(sys.argv):
    arg = sys.argv[index]
    index += 1
    if arg in ['-h', '-H','--help']:
        print_help()
        sys.exit(0)
    elif arg in ['--rusprofile']:
        rusprofile_requests = True
    elif target_file == '':
        target_file = arg
    elif output_file == '':
        output_file = arg
    elif validate_inn(arg):
        suspects.append(arg)
    else:
        print("%s is not a valid inn" % arg)
        sys.exit(1)

if len(suspects) < 2:
    print ("At least 2 suspects needed! \n Not much of a cartel otherwise!")
    sys.exit(1)
# maybe someday output_file = target_file

transform_table(target_file, output_file, suspects)
sys.exit(0)