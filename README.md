## Requirements (for .py):

* Python 3.x
* openpyxl library:

`$ pip install openpyxl`

## Usage:

This is a CLI application meant to be called from terminal or another program.
You can receive actual reference by using builtin help:

### Unix-like:

`$ ./filter.py --help`

### Windows (for compiled exe. Probably, untested outside of wine):

`filter.exe --help`
